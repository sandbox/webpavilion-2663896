/**
 * @file
 * field filter listing behaviors.
 */

(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.FieldFilterByText = {
    attach: function (context, settings) {

      var $input = $('input.field-filter-text').once('.field-filter-text');
      var $table = $($input.attr('data-table'));
      var $rows;

      function filterViewList(e) {
        var query = $(e.target).val().toLowerCase();

        function showRow(index, row) {
          var $row = $(row);
          var $sources = $row.find('.field-filter-text-source');
          var textMatch = $sources.text().toLowerCase().indexOf(query) !== -1;
          $row.closest('tr').toggle(textMatch);
        }

        if (query.length >= 1) {
          $rows.each(showRow);
        } else {
          $rows.show();
        }
        
      }

      if ($table.length) {
        $rows = $table.find('tbody tr');
        $input.on('keyup', filterViewList);
      }
    }
  };

}(jQuery, Drupal));
