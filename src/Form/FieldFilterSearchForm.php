<?php

/**
 * @file
 * Contains \Drupal\field_filter\Form\field_filter_search_form.
 */

namespace Drupal\field_filter\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class FieldFilterSearchForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'field_filter_search_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['field_filter_search.settings'];
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['filters'] = array(
      '#type' => 'search',
      '#title' => $this->t('Filter field'),
      '#title_display' => 'invisible',
      '#size' => 50,
      '#placeholder' => $this->t('Filter by label, machine name or field type'),
      '#attributes' => array(
        'class' => array('field-filter-text'),
        'data-table' => '#field-overview',
        'autocomplete' => 'off',
      ),
    );

    $form['#attached']['library'][] = 'field_filter/field_filter.listing';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
