<?php

/**
 * @file
 * Contains \Drupal\field_filter\Form\field_filterForm.
 */

namespace Drupal\field_filter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class FieldFilterForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'field_filter_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['field_filter.settings'];
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('field_filter.settings');

    $form['info_column'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Show info column (cardinality and required)'),
      '#default_value' => $config->get('field_filter.info_column'),
    );
    $form['length_column'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Show length column for text fields'),
      '#default_value' => $config->get('field_filter.length_column'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $config = $this->config('field_filter.settings');

    foreach (array('info', 'length') as $name) {
      $name = "{$name}_column";
      $config->set("field_filter.{$name}", $values[$name]);
    }

    $config->save();
  }

}
