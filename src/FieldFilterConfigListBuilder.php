<?php

/**
 * @file
 * Contains \Drupal\field_filter\FieldFilterConfigListBuilder.
 */

namespace Drupal\field_filter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\field_ui\FieldConfigListBuilder;

/**
 * Provides lists of field config entities.
 */
class FieldFilterConfigListBuilder extends FieldConfigListBuilder {

  protected $FieldFilterConfig;

  /**
   *  Constructs a new class instance.
   *
   * @param EntityTypeInterface $entity_type
   * @param EntityManagerInterface $entity_manager
   * @param FieldTypePluginManagerInterface $field_type_manager
   */
  public function __construct(EntityTypeInterface $entity_type, EntityManagerInterface $entity_manager, FieldTypePluginManagerInterface $field_type_manager) {

    $this->FieldFilterConfig = \Drupal::config('field_filter.settings');

    parent:: __construct($entity_type, $entity_manager, $field_type_manager);
  }

  /**
   * Build Header
   */
  public function buildHeader() {

    $add = array();

    if ($this->FieldFilterConfig->get('field_filter.info_column')) {
      $add['info']['data'] = $this->t('Info');
    }

    if ($this->FieldFilterConfig->get('field_filter.length_column')) {
      $add['length']['data'] = $this->t('Length');
    }

    return $add + parent::buildHeader();
  }

  /**
   * Build Row
   */
  public function buildRow(EntityInterface $field_config) {
    $add = array();

    $field_storage = $field_config->getFieldStorageDefinition();

    if ($this->FieldFilterConfig->get('field_filter.info_column')) {
      //&infin;?
      $cardinality['data'] = $field_storage->getCardinality() == -1 ? '∞' : $field_storage->getCardinality();

      if ($field_config->isRequired() == 1) {
        $cardinality['class'][] = 'form-required';
      }

      $add['field_info'] = $cardinality;
    }

    if ($this->FieldFilterConfig->get('field_filter.length_column')) {
      $add['field_size'] = $field_storage->getSetting('max_length');
    }

    $row = parent::buildRow($field_config);

    foreach ($row['data'] as $name => &$value) {
      if ($name != 'operations') {

        if (isset($value['data'])) {
          $value['class'][] = 'field-filter-text-source';
        }
        else {
          $value = array(
            'data' => $value,
            'class' => array('field-filter-text-source'),
          );
        }
      }
    }

    $row['data'] = $add + $row['data'];

    return $row;
  }

  /**
   * Render list
   * @param type $target_entity_type_id
   * @param type $target_bundle
   */
  public function render($target_entity_type_id = NULL, $target_bundle = NULL) {
    $add['field_filter'] = \Drupal::formBuilder()->getForm('Drupal\field_filter\Form\FieldFilterSearchForm');

    return $add + parent::render($target_entity_type_id, $target_bundle);
  }

}
